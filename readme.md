# Almacén de zapatos Calzame

## Descripcion

Nuestra pagina brinda toda la informacion sobre nuestra tienda de zapatos que en este momento tiene unas muy buenas promociones que se deben aprovechar

>Si quieres saber mas visitanos
[Pagina Web](https://reliable-shortbread-94f184.netlify.app)

## Como clonar
> Para clonar el proyecto brindamos las siguientes 3 opciones.
 * **Clone with SSH:** 
 ```
 git@gitlab.com:ntrujillo74950/tienda_de_zapatos.git
 ```
 * **Clone with HTTPS:** 
 ```
https://gitlab.com/ntrujillo74950/tienda_de_zapatos.git
 ```

## Autores
> Valentina Diaz Orozco

> Nicolas Trujillo Rios

# Inicio

Este modulo nos ofrece la oportunidad de ver la principal actividad y los zapatos de la ultima coleccion.

# Registro

Este modulo nos ofrece la oportunidad de registrarse para obtener mejores promociones.

# Promociones

Este modulo nos da la opcion de ver las promociones disponibles.
